## Docker for Windows
https://docs.docker.com/docker-for-windows/install/#what-to-know-before-you-install

## About Dockerfile
https://docs.docker.com/get-started/part2/#define-a-container-with-dockerfile

```
docker image build -t tshark .
docker container run tshark -h
```

## Install kubectl
https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-with-chocolatey-on-windows

## Tutorial
* https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-interactive/
* https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/explore-interactive/
* https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-interactive/


## kubectl Cheat Sheet
https://kubernetes.io/docs/reference/kubectl/cheatsheet/


## Simple pod example
```bash
kubectl create -f pod.yaml
```