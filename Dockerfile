FROM alpine

RUN apk add --no-cache tshark coreutils

USER root:root

RUN mkdir /data

CMD ["tshark"]